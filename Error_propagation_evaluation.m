% Author:   M. Fonseca, R. Dumas
%           February 2021

% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/ (...) ---/!\ to be added --
% Reference  :   "An analytical model to quantify the impact of uncertainty propagation in the knee joint angle computation" 
%                M. Fonseca, R. Dumas, S. Armand; 
% Date       :   --(to be added after publication)--
% -------------------------------------------------------------------------
% Description:   Routine to evaluate the error propagation on the
% definition of the Euler angles of rotation
% Process    :   1 - Definition of the symbolic function to calculate
% uncertainty of theta1,theta2, theta3 using syms module(symbolic equations)
%                2 - Load data
%                3 - Intra-session data
%                4 - Inter-session data
%                5 - Replace input variables in the function of squared standard uncertainty (subs)
%                6 - Plot theta1 results togheter with a theoretical corridor (mean_theta, std)
% -------------------------------------------------------------------------


clear all
close all
clc
% cd 'H:\Backup pc HUG 1Drive\Gitlab\Helical Axis\Code'

%% User input
% Plot title
title_plot = 'U = 10';
% Theoretical standard uncertainty input for each of the input parameters
theoretical_error_theta = 5;
theoretical_error_k = 5;
theoretical_error_e1 = 5;
theoretical_error_e3 = 5;
%% 1. Definition of the symbolic function to calculate uncertainty of theta1,theta2, theta3 using syms
% 1.1 symbolic variable names
syms theta kx ky e1x e1y e3x e3z real
syms ssu_theta ssu_kx ssu_ky real
syms ssu_e1x ssu_e1y real
syms ssu_e3x ssu_e3z real
% 1.2 define spatial vectors
k = [kx, ky, sqrt(1-kx^2-ky^2)]; 
e1 = [e1x, e1y, sqrt(1-e1x^2-e1y^2)];
assume(e1, 'real')
e3 = [e3x, sqrt(1-e3x^2-e3z), e3z];
assume(e3, 'real')
e2 = cross(e3,e1)/sqrt(sum(cross(e3,e1).^2,2));
% 1.3 symbolic equations for the calculation of uncertainty of each theta
% Theta1 -----------------------------------------------------------------
ftheta1 = dot(cross(e2,e3),k)*theta;
gtheta1 = dot(cross(e1,e2),e3);
%
dftheta1dtheta = gradient(ftheta1,theta);
dftheta1dkx = gradient(ftheta1,kx);
dftheta1dky = gradient(ftheta1, ky);
dftheta1de1x = gradient(ftheta1, e1x);
dftheta1de1y = gradient(ftheta1, e1y);
dftheta1de3x = gradient(ftheta1,e3x); 
dftheta1de3z = gradient(ftheta1,e3z);

%
dgtheta1dtheta = gradient(gtheta1,theta);
dgtheta1dkx = gradient(gtheta1,kx);
dgtheta1dky = gradient(gtheta1, ky);
dgtheta1de1x = gradient(gtheta1, e1x);
dgtheta1de1y = gradient(gtheta1, e1y);
dgtheta1de3x = gradient(gtheta1, e3x);
dgtheta1de3z = gradient(gtheta1, e3z);

ssu_theta1 = (1/gtheta1)^4*...
        ((gtheta1*dftheta1dtheta - ftheta1*dgtheta1dtheta)^2*ssu_theta + ...
        (gtheta1*dftheta1dkx - ftheta1*dgtheta1dkx)^2*ssu_kx + ...
        (gtheta1*dftheta1dky - ftheta1*dgtheta1dky)^2*ssu_ky + ...
        (gtheta1*dftheta1de1x - ftheta1*dgtheta1de1x)^2*ssu_e1x + ...
        (gtheta1*dftheta1de1y - ftheta1*dgtheta1de1y)^2*ssu_e1y + ...
        (gtheta1*dftheta1de3x - ftheta1*dgtheta1de3x)^2*ssu_e3x + ...
        (gtheta1*dftheta1de3z - ftheta1*dgtheta1de3z)^2*ssu_e3z);
% Theta2 ------------------------------------------------------------------
ftheta2 = dot(cross(e1,e3),k)*theta;
gtheta2 = dot(cross(e1,e2),e3);
%
dftheta2dtheta = gradient(ftheta2,theta);
dftheta2dkx = gradient(ftheta2,kx);
dftheta2dky = gradient(ftheta2, ky);
dftheta2de1x = gradient(ftheta2, e1x);
dftheta2de1y = gradient(ftheta2, e1y);
dftheta2de3x = gradient(ftheta2,e3x); 
dftheta2de3z = gradient(ftheta2,e3z);
%
dgtheta2dtheta = gradient(gtheta2,theta);
dgtheta2dkx = gradient(gtheta2,kx);
dgtheta2dky = gradient(gtheta2, ky);
dgtheta2de1x = gradient(gtheta2, e1x);
dgtheta2de1y = gradient(gtheta2, e1y);
dgtheta2de3x = gradient(gtheta2, e3x);
dgtheta2de3z = gradient(gtheta2, e3z);

ssu_theta2 = (1/gtheta2)^4*...
        ((gtheta2*dftheta2dtheta - ftheta2*dgtheta2dtheta)^2*ssu_theta + ...
        (gtheta2*dftheta2dkx - ftheta2*dgtheta2dkx)^2*ssu_kx + ...
        (gtheta2*dftheta2dky - ftheta2*dgtheta2dky)^2*ssu_ky + ...
        (gtheta2*dftheta2de1x - ftheta2*dgtheta2de1x)^2*ssu_e1x + ...
        (gtheta2*dftheta2de1y - ftheta2*dgtheta2de1y)^2*ssu_e1y + ...
        (gtheta2*dftheta2de3x - ftheta2*dgtheta2de3x)^2*ssu_e3x + ...
        (gtheta2*dftheta2de3z - ftheta2*dgtheta2de3z)^2*ssu_e3z);

% Theta3 ------------------------------------------------------------------
ftheta3 = dot(cross(e1,e2),k)*theta;
gtheta3 = dot(cross(e1,e2),e3);
%
dftheta3dtheta = gradient(ftheta3,theta);
dftheta3dkx = gradient(ftheta3,kx);
dftheta3dky = gradient(ftheta3, ky);
dftheta3de1x = gradient(ftheta3, e1x);
dftheta3de1y = gradient(ftheta3, e1y);
dftheta3de3x = gradient(ftheta3,e3x); 
dftheta3de3z = gradient(ftheta3,e3z);
%
dgtheta3dtheta = gradient(gtheta3,theta);
dgtheta3dkx = gradient(gtheta3,kx);
dgtheta3dky = gradient(gtheta3, ky);
dgtheta3de1x = gradient(gtheta3, e1x);
dgtheta3de1y = gradient(gtheta3, e1y);
dgtheta3de3x = gradient(gtheta3, e3x);
dgtheta3de3z = gradient(gtheta3, e3z);

ssu_theta3 = (1/gtheta3)^4*...
        ((gtheta3*dftheta3dtheta - ftheta3*dgtheta3dtheta)^2*ssu_theta + ...
        (gtheta3*dftheta3dkx - ftheta3*dgtheta3dkx)^2*ssu_kx + ...
        (gtheta3*dftheta3dky - ftheta3*dgtheta3dky)^2*ssu_ky + ...
        (gtheta3*dftheta3de1x - ftheta3*dgtheta3de1x)^2*ssu_e1x + ...
        (gtheta3*dftheta3de1y - ftheta3*dgtheta3de1y)^2*ssu_e1y + ...
        (gtheta3*dftheta3de3x - ftheta3*dgtheta3de3x)^2*ssu_e3x + ...
        (gtheta3*dftheta3de3z - ftheta3*dgtheta3de3z)^2*ssu_e3z);
%% 2. Load reference data
load('Parameters_VAR.mat');
load('Parameters_intrinsic_VAR.mat');

%% 3. Intra session data
% Get parameters
theta_vi = RIF.theta;
kx_vi = RIF.kx;
ky_vi = RIF.ky;
e1x_vi = RIF.e1x;
e1y_vi = RIF.e1y;
e3x_vi = RIF.e3x;
e3z_vi = RIF.e3z;
% Display
disp(['Mean experimental SD on theta: ',num2str(mean(rad2deg(std(theta_vi,[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on kx: ',num2str(mean(rad2deg(std(atan(kx_vi),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on ky: ',num2str(mean(rad2deg(std(atan(ky_vi),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on e3x: ',num2str(mean(rad2deg(std(atan(e3x_vi),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on e3z: ',num2str(mean(rad2deg(std(atan(e3z_vi),[],2))))]); % Experimental standard deviation
for t = 1:size(theta_vi,2)
    theta1_res_i(:,t) = double(subs(ftheta1/gtheta1,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_vi(:,t),kx_vi(:,t),ky_vi(:,t),e1x_vi(:,t),e1y_vi(:,t),e3x_vi(:,t),e3z_vi(:,t)}));
    
    theta2_res_i(:,t) = double(subs(ftheta2/gtheta2,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_vi(:,t),kx_vi(:,t),ky_vi(:,t),e1x_vi(:,t),e1y_vi(:,t),e3x_vi(:,t),e3z_vi(:,t)}));
    
    theta3_res_i(:,t) = double(subs(ftheta3/gtheta3,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_vi(:,t),kx_vi(:,t),ky_vi(:,t),e1x_vi(:,t),e1y_vi(:,t),e3x_vi(:,t),e3z_vi(:,t)}));   
end
%% 4. Inter session data 
% Get parameters
theta_v = REF.theta;
kx_v = REF.kx;
ky_v = REF.ky;
e1x_v = REF.e1x;
e1y_v = REF.e1y;
e3x_v = REF.e3x;
e3z_v = REF.e3z;
% Display
disp(['Mean experimental SD on theta: ',num2str(mean(rad2deg(std(theta_v,[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on kx: ',num2str(mean(rad2deg(std(atan(kx_v),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on ky: ',num2str(mean(rad2deg(std(atan(ky_v),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on e3x: ',num2str(mean(rad2deg(std(atan(e3x_v),[],2))))]); % Experimental standard deviation
disp(['Mean experimental SD on e3z: ',num2str(mean(rad2deg(std(atan(e3z_v),[],2))))]); % Experimental standard deviation
for t = 1:size(theta_v,2)
    theta1_res(:,t) = double(subs(ftheta1/gtheta1,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_v(:,t),kx_v(:,t),ky_v(:,t),e1x_v(:,t),e1y_v(:,t),e3x_v(:,t),e3z_v(:,t)}));
    
    theta2_res(:,t) = double(subs(ftheta2/gtheta2,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_v(:,t),kx_v(:,t),ky_v(:,t),e1x_v(:,t),e1y_v(:,t),e3x_v(:,t),e3z_v(:,t)}));
    
    theta3_res(:,t) = double(subs(ftheta3/gtheta3,...
        {theta,kx,ky,e1x,e1y,e3x,e3z,}, ...
        {theta_v(:,t),kx_v(:,t),ky_v(:,t),e1x_v(:,t),e1y_v(:,t),e3x_v(:,t),e3z_v(:,t)}));   
end
%% 5. Replace input variables in the function of squared standard uncertainty (subs)
% Uncertainty
% Squared standard uncertainty ((standard deviation)^2)
utheta = deg2rad(theoretical_error_theta)^2;
uk = deg2rad(theoretical_error_k)^2;
ue1 = deg2rad(theoretical_error_e1)^2;
ue3 = deg2rad(theoretical_error_e3)^2;

% Define ssu for each imput variable
% Intrinsic
ssu_theta_v = utheta;
ssu_kx_v = tan(uk); ssu_ky_v = tan(uk);
% Extrinsic
ssu_e1x_v = tan(ue1); ssu_e1y_v = tan(ue1);
ssu_e3x_v =tan(ue3); ssu_e3z_v = tan(ue3);

% Define ssu for theta 1
ssu_theta1_res = double(subs(ssu_theta1, ...
    {theta,kx,ky,e1x,e1y,e3x,e3z,...
    ssu_e1x,ssu_e1y,ssu_e3x,ssu_e3z, ssu_theta, ssu_kx, ssu_ky}, ...
    {mean(theta_v,2),mean(kx_v,2),mean(ky_v,2),mean(e1x_v,2),mean(e1y_v,2),mean(e3x_v,2),mean(e3z_v,2),...
    ssu_e1x_v,ssu_e1y_v,ssu_e3x_v,ssu_e3z_v, ssu_theta_v, ssu_kx_v, ssu_ky_v}));

% Define ssu for theta 2
ssu_theta2_res = double(subs(ssu_theta2, ...
    {theta,kx,ky,e1x,e1y,e3x,e3z,...
    ssu_e1x,ssu_e1y,ssu_e3x,ssu_e3z, ssu_theta, ssu_kx, ssu_ky}, ...
    {mean(theta_v,2),mean(kx_v,2),mean(ky_v,2),mean(e1x_v,2),mean(e1y_v,2),mean(e3x_v,2),mean(e3z_v,2),...
    ssu_e1x_v,ssu_e1y_v,ssu_e3x_v,ssu_e3z_v, ssu_theta_v, ssu_kx_v, ssu_ky_v}));

% Define ssu for theta 3
ssu_theta3_res = double(subs(ssu_theta3, ...
    {theta,kx,ky,e1x,e1y,e3x,e3z,...
    ssu_e1x,ssu_e1y,ssu_e3x,ssu_e3z, ssu_theta, ssu_kx, ssu_ky}, ...
    {mean(theta_v,2),mean(kx_v,2),mean(ky_v,2),mean(e1x_v,2),mean(e1y_v,2),mean(e3x_v,2),mean(e3z_v,2),...
    ssu_e1x_v,ssu_e1y_v,ssu_e3x_v,ssu_e3z_v, ssu_theta_v, ssu_kx_v, ssu_ky_v}));

%% 6. Plot theta1 results togheter with a theoretical corridor (mean_theta, std)
h = figure(1)
sp1 = subplot(3,1,1)
A = real(rad2deg(mean(theta1_res,2))); %extrinsic variability
Ai= real(rad2deg(mean(theta1_res_i,2))); %intrinsic variability
B = real(rad2deg(std(theta1_res,[],2)));
Bi= real(rad2deg(std(theta1_res_i,[],2)));
Max = A+B; Min = A-B;
Maxi= Ai+Bi; Mini = Ai-Bi;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
yi= [Maxi;Mini(end:-1:1)];
hold on
plot(A,'Color',[0 0.4470 0.7410],'linestyle','-','Marker','none');
plot(Ai, 'Color', [0.7410 0.4470 0], 'linestyle','-', 'Marker', 'none'); 
fill(x,y,[0 0.4470 0.7410] , 'linestyle','-','edgecolor','none','faceAlpha',0.5)
fill(x,yi,[0 0.740 0], 'linestyle','-','edgecolor','none','faceAlpha',0.5)
xlim([1 101]);
B = real(rad2deg(sqrt(ssu_theta1_res))); % Standard uncertainty
Max = A+B; Min = A-B;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
fill(x,y,[0.8500 0.3250 0.0980],'linestyle','-','edgecolor','none','faceAlpha',0.5)
ylabel('Theta 1')
title(title_plot)
sp1.XAxis.TickValues = [0 20 40 60 80 100];
sp1.FontSize = 20

sp2 = subplot(3,1,2)
A = real(rad2deg(mean(theta2_res,2)));
Ai= real(rad2deg(mean(theta2_res_i,2)));
B = real(rad2deg(std(theta2_res,[],2)));
Bi= real(rad2deg(std(theta2_res_i,[],2)));
Max = A+B; Min = A-B;
Maxi= Ai+Bi; Mini = Ai-Bi;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
yi= [Maxi;Mini(end:-1:1)];
hold on
plot(A,'Color',[0 0.4470 0.7410],'linestyle','-','Marker','none');
plot(Ai,'Color',[0.7410 0.4470 0],'linestyle','-','Marker','none');
fill(x,y,[0 0.4470 0.7410],'linestyle','-','edgecolor','none','faceAlpha',0.5)
fill(x,yi,[0 0.740 0],'linestyle','-','edgecolor','none','faceAlpha',0.5)
xlim([1 101]);
B = real(rad2deg(sqrt(ssu_theta2_res))); % Standard uncertainty (theoretical variability)
Max = A+B; Min = A-B;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
fill(x,y,[0.8500 0.3250 0.0980],'linestyle','-','edgecolor','none','faceAlpha',0.5)
ylabel('Theta 2')
sp2.XAxis.TickValues = [0 20 40 60 80 100];
sp2.FontSize = 20

sp3 = subplot(3,1,3)
A = real(rad2deg(mean(theta3_res,2)));
Ai= real(rad2deg(mean(theta3_res_i,2)));
B = real(rad2deg(std(theta3_res,[],2)));
Bi= real(rad2deg(std(theta3_res_i,[],2)));
Max = A+B; Min = A-B;
Maxi= Ai+Bi; Mini=Ai-Bi;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
yi= [Maxi;Mini(end:-1:1)];
hold on
plot(A,'Color',[0 0.4470 0.7410],'linestyle','-','Marker','none');
plot(Ai,'Color', [0.7410 0.4470 0], 'linestyle','-','Marker','none');
fill(x,y,[0 0.4470 0.7410],'linestyle','-','edgecolor','none','faceAlpha',0.5)
fill(x,yi,[0 0.740 0],'linestyle','-','edgecolor','none','faceAlpha',0.5)
xlim([1 101]);
B = real(rad2deg(sqrt(ssu_theta3_res))); % Standard uncertainty
Max = A+B; Min = A-B;
x = [1:101 101:-1:1];
y = [Max;Min(end:-1:1)];
fill(x,y,[0.8500 0.3250 0.0980],'linestyle','-','edgecolor','none','faceAlpha',0.5)
ylabel('Theta 3')
xlabel('% of Gait Cycle')
legend({'Experimental Inter-Session(mean)','Experimental Intra-Session(mean)','Experimental Inter-Session(SD)' ,'Experimental Intra-Session(SD)','Theoretical (SD)'})
sp3.XAxis.TickValues = [0 20 40 60 80 100];
sp3.FontSize = 20

h = gcf;
set(h, 'PaperPositionMode', 'auto');
set(h, 'PaperOrientation', 'landscape');
% set(h, 'Position', [50 50 1200 800]);
set(gcf, 'Units', 'Normalized','OuterPosition', [0,0,1,1])
print(gcf, '-dpdf', 'SSU10.pdf')



